import React from "react";
import {AiFillFacebook,AiFillTwitterCircle,AiFillInstagram,AiFillSkype,AiFillHeart} from "react-icons/ai"
import {FaPinterestP} from "react-icons/fa"
import "../../Style/footer.css"
const Footer = () => {
    return ( 
        <footer className="footer">
		<div className="container">
			<div className="row">
				<div className="col-lg-6">
					<div className="footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center">
						<ul className="footer_nav">
							<li><a href="/">Blog</a></li>
							<li><a href="/">FAQs</a></li>
							<li><a href="contact.html">Contact us</a></li>
						</ul>
					</div>
				</div>
				<div className="col-lg-6">
					<div className="footer_social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
						<ul>
                            <li><a href="/"><AiFillFacebook/></a></li>
							<li><a href="/"><AiFillTwitterCircle/></a></li>
							<li><a href="/"><AiFillInstagram/></a></li>
							<li><a href="/"><AiFillSkype/></a></li>
							<li><a href="/"><FaPinterestP/></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div className="row">
				<div className="col-lg-12">
					<div className="footer_nav_container">
						<div className="cr">©2018 All Rights Reserverd. Made with <AiFillHeart/> by <a href="/">Colorlib</a> &amp; distributed by <a href="https://themewagon.com">ThemeWagon</a></div>
					</div>
				</div>
			</div>
		</div>
	</footer>
     );
}
 
export default Footer;