import React, { useEffect, useState } from "react";
import axios from "axios";
import "../../Style/Arrivals.css";
import {useDispatch} from "react-redux";
import { showItem } from "../../Redux/productSlice";
import {Link} from "react-router-dom"
const Arrival = () => {
    const [dataArrival,setDataArrival]=useState([])
    const dispatch=useDispatch()
    const handleClick=(item)=>{
        dispatch(showItem(item))
    }
    const fetch=async()=>{
        await axios({
            method:"GET",
            url:"https://632745af5731f3db995690a6.mockapi.io/product"
        })
        .then((res)=>{
            const data=res.data.slice(10)
            setDataArrival(data)
        })
        .catch((error)=>{
            console.log(error)
        })
    }
    useEffect(()=>{
        fetch()
    },[])
    return ( 
        <div className="new_arrivals">
            <div className="container">
                <div className="row">
                    <div className="col text-center">
                        <div className="section_title new_arrivals_title">
                            <h2>Các sản phẩm mới nhất</h2>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <div className="product-grid">
                            {dataArrival.map((item,index)=>(
                                <div className="product-item men" key={index}>
                                    <div className="product discount product_filter">
                                        <div className="product_image">
                                            <img src={item.image[0]} alt=""/>
                                        </div>
                                        <div className="product_info">
                                            <h6 className="product_name">{item.title}</h6>
                                            <div className="product_price">${item.price}</div>
                                        </div>
                                    </div>
                                    <div onClick={()=>handleClick(item)} className="red_button add_to_cart_button">
                                            <Link to="/product" alt="">Chi tiết</Link>
                                        </div>
                                </div>
                            ))}
                        </div>
                    </div>
			    </div>
            </div>
        </div>
       
     );
}
 
export default Arrival;