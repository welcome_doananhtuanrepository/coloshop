import React,{useState,useEffect, useRef} from "react";
import "../../Style/deal.css"
import dealImage from "../../asset/images/deal_ofthe_week.png"
import {Link} from "react-router-dom"
const Deal = () => {
    const [day,setDay]=useState("00")
    const [hours,setHours]=useState("00")
    const [minutes,setMinutes]=useState("00")
    const [seconds, setSeconds]=useState("00")

    let interval=useRef()

    const startTimer=()=>{
        const countDowntimer=new Date("Oct 23, 2022 00:00:00").getTime()

        interval=setInterval(()=>{
            const now=new Date().getTime()
            const distance=countDowntimer-now
            const day=Math.floor(distance/(1000*60*60*24))
            const hours=Math.floor(distance%(1000*60*60*24)/(1000*60*60))
            const minutes=Math.floor(distance%(1000*60*60)/(1000*60))
            const seconds=Math.floor(distance%(1000*60)/1000)
            if(distance<0){
                clearInterval(interval.current)
            }else{
                setDay(day)
                setHours(hours)
                setMinutes(minutes)
                setSeconds(seconds)
            }
        },1000)
    }
    useEffect(()=>{
        startTimer()
        return ()=>{
            clearInterval(interval.current)
        }
    },[])
    return ( 
        <div className="deal_ofthe_week">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <div className="deal_ofthe_week_img">
                            <img style={{width:"90%"}} src={dealImage} alt=""/>
                        </div>
                    </div>
                    <div className="col-lg-6 text-right deal_ofthe_week_col">
                        <div className="deal_ofthe_week_content d-flex flex-column align-items-center float-right">
                            <div className="section_title">
                                <h2>Sự kiện sắp diễn ra</h2>
                            </div>
                            <ul className="timer">
                                <li className="d-inline-flex flex-column justify-content-center align-items-center">
                                    <div id="day" className="timer_num">{day}</div>
                                    <div className="timer_unit">Ngày</div>
                                </li>
                                <li className="d-inline-flex flex-column justify-content-center align-items-center">
                                    <div id="hour" className="timer_num">{hours}</div>
                                    <div className="timer_unit">Giờ</div>
                                </li>
                                <li className="d-inline-flex flex-column justify-content-center align-items-center">
                                    <div id="minute" className="timer_num">{minutes}</div>
                                    <div className="timer_unit">Phút</div>
                                </li>
                                <li className="d-inline-flex flex-column justify-content-center align-items-center">
                                    <div id="second" className="timer_num">{seconds}</div>
                                    <div className="timer_unit">Giây</div>
                                </li>
                            </ul>
                            <Link to="/products" className="red_button deal_ofthe_week_button" style={{color:"white"}}>Mua hàng</Link>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
     );
}
 
export default Deal;