import React, { useState } from "react";
import "./navbar.css"
import {AiOutlineUserAdd,AiOutlineShoppingCart,AiOutlineMenu,AiOutlineClose} from "react-icons/ai"
import {Link} from "react-router-dom";
import { useSelector} from 'react-redux'
const Navbar = () => {
    const [open,setOpen]=useState(true)
    const itemCart=useSelector(state=>state.cart.cart)
    return ( 
        <nav>
            <div className="menu">
                <div>
                    <h1>COLO<span style={{color:"red"}}>SHOP</span></h1>
                </div>
                <div className="content">
                    <ul className="listMenu" style={{left:`${open?"-100%":"0"}`}}>
                        <AiOutlineClose className="button btnclose" size={"26px"} onClick={()=>setOpen(true)}/>
                        <li><Link to="/" alt="">Trang chủ</Link></li>
                        <li><Link to="/products" alt="">Sản Phẩm</Link></li>
                        <li><Link to="/contact" alt="">Liên hệ</Link></li>
                    </ul>
                    <ul className="navbar_user">
						<li><Link to="/user"><AiOutlineUserAdd/></Link></li>
						<li className="checkout">
							<Link to="/cart">
								<AiOutlineShoppingCart/>
								<span id="checkout_items" className="checkout_items">{itemCart.length}</span>
							</Link>
						</li>
					</ul>
                    <div className="button btnmenu">
                        <AiOutlineMenu className="iconMenu" onClick={()=>setOpen(false)}/>
                    </div>
                </div>
            </div>
        </nav>
     );
}
 
export default Navbar;