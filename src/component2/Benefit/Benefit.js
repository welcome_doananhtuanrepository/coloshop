import React from "react";
import "../../Style/benefit.css";
import {MdLocalShipping,MdKeyboardReturn} from "react-icons/md";
import {FaMoneyBillAlt,FaClock} from "react-icons/fa";
const Benefit = () => {
    return ( 
        <div className="benefit">
            <div className="container">
                <div className="row benefit_row">
                    <div className="col-lg-3 benefit_col">
                        <div className="benefit_item d-flex flex-row align-items-center">
                            <div className="benefit_icon"><MdLocalShipping className="benefit_icon"/></div>
                            <div className="benefit_content">
                                <h6>free shipping</h6>
                                <p>Suffered Alteration in Some Form</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 benefit_col">
                        <div className="benefit_item d-flex flex-row align-items-center">
                            <div className="benefit_icon"><FaMoneyBillAlt className="benefit_icon"/></div>
                            <div className="benefit_content">
                                <h6>cach on delivery</h6>
                                <p>The Internet Tend To Repeat</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 benefit_col">
                        <div className="benefit_item d-flex flex-row align-items-center">
                            <div className="benefit_icon"><MdKeyboardReturn className="benefit_icon"/></div>
                            <div className="benefit_content">
                                <h6>45 days return</h6>
                                <p>Making it Look Like Readable</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 benefit_col">
                        <div className="benefit_item d-flex flex-row align-items-center">
                            <div className="benefit_icon"><FaClock className="benefit_icon"/></div>
                            <div className="benefit_content">
                                <h6>opening all week</h6>
                                <p>8AM - 09PM</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
     );
}
 
export default Benefit;