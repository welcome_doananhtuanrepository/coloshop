import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css"; 
import "../../Style/bestSeller.css"
import {dataSlider} from "./data"
const BestSeller = () => {
    const settings = {
        // dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
            //   dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: true,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: true,
              slidesToScroll: 1
            }
          }
        ]
      };
    return ( 
        <div className="section">
            <h2 className="BestSeller">Sản phẩm bán chạy</h2>
            <div className="slider">
                <Slider {...settings}>
                    {dataSlider.map((item,index)=>(
                        <div className="cart" key={index}>
                            <div className="cart_image">
                                <img className="cart_img" src={item.linkImg} alt=""/>
                            </div>
                            <h6 className="cart_title">{item.title}</h6>
                            <button className="cart_btn--green">Xem chi tiết</button>
                        </div>
                    ))}
                </Slider>
            </div>
        </div>
     );
}
 
export default BestSeller;