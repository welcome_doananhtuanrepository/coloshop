
export const dataSlider = [
    {
      id: 1,
      title: 'Áo thun nam trắng Mockup',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/free-psd/white-t-shirt-model-front-view-mockup_125540-861.jpg?w=740&t=st=1663321512~exp=1663322112~hmac=8e4f866a4f0dd32b27718355200deee208e20f4384c53a905afdb1cb7d6690c6"
    },
    {
      id: 2,
      title: 'Áo len nữ mùa đông',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/free-photo/green-front-sweater_125540-736.jpg?size=626&ext=jpg&ga=GA1.2.1958386843.1659421973"
    },
    {
      id: 3,
      title: 'Áo khoác công sở nữ',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/free-photo/fashion-portrait-young-elegant-woman_1328-2685.jpg?size=338&ext=jpg&ga=GA1.2.1958386843.1659421973"
    },
    {
      id: 4,
      title: 'Áo khoác nữa xám',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/premium-photo/jacket_87394-24396.jpg?size=626&ext=jpg&ga=GA1.2.1958386843.1659421973"
    },
    {
      id: 5,
      title: 'Áo thun đen trơn nam',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/free-photo/isolated-opened-black-t-shirt_125540-1451.jpg?size=626&ext=jpg&ga=GA1.2.1958386843.1659421973"
    },
    {
      id: 6,
      title: 'Đầm nữ công sở',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/free-photo/fashion-woman-with-clothes_1203-8301.jpg?size=338&ext=jpg&ga=GA1.2.1958386843.1659421973"
    },
    {
      id: 7,
      title: 'Nhẫn đính kim cương',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/free-photo/jewels-sparkle-golden-wedding-rings-lying-leather_8353-763.jpg?size=626&ext=jpg&ga=GA1.2.1958386843.1659421973"
    },
    {
      id: 8,
      title: 'Nhẫn đôi cho couple',
      price: '$59.99',
      category: 'male',
      linkImg:"https://img.freepik.com/free-photo/closeup-shot-two-diamond-rings-white-surface_181624-47070.jpg?size=626&ext=jpg&ga=GA1.2.1958386843.1659421973"
    },
  ];