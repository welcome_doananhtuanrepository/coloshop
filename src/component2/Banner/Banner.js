import React from "react";
import "../../Style/banner.css";
import banner1 from "../../asset/images/banner_1.jpg";
import banner2 from "../../asset/images/banner_2.jpg";
import banner3 from "../../asset/images/banner_3.jpg";
const Banner = () => {
    return ( 
        <div className="banner">
            <div className="container">
                <div className="row">
                    <div className="col-md-4 banner_item_box">
                        <div className="banner_item align-items-center" style={{backgroundImage:`url(${banner1})`}}>
                            <div className="banner_category">
                                <a href="categories.html">Thời trang nữ</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 banner_item_box">
                        <div className="banner_item align-items-center" style={{backgroundImage:`url(${banner2})`}}>
                            <div className="banner_category">
                                <a href="categories.html">Phụ kiện</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 banner_item_box">
                        <div className="banner_item align-items-center" style={{backgroundImage:`url(${banner3})`}}>
                            <div className="banner_category">
                                <a href="categories.html">Thời trang nam</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
     );
}
 
export default Banner;