import React from "react";
import "../../Style/User.css";
import { useForm } from "react-hook-form";
const User = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    console.log(data);
  };
  return (
    <div className="User">
      <form onSubmit={handleSubmit(onSubmit)}>
        <h1>Đăng ký thành viên</h1>
        <label htmlFor="name">Tên</label>
        <input
          type="text"
          name="name"
          id="name"
          placeholder="Your Name"
          {...register("name", {
            required: true,
            pattern: /(^[a-zA-Z][a-zA-Z\s]{0,20}[a-zA-Z]$)/,
          })}
        />
        <label htmlFor="name">Mật khẩu</label>
        <input
          type="password"
          name="password"
          id="password"
          placeholder="Password"
          {...register("password", { required: true, minLength: 6 })}
        />
        <label htmlFor="email">Email</label>
        <input
          type="email"
          name="email"
          id="email"
          placeholder="Your Email"
          {...register("email", {
            required: true,
            pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
          })}
        />

        {Object.keys(errors).length !== 0 && (
          <ul className="error-container">
            {errors.name?.type === "required" && <li>Name is required</li>}
            {errors.name?.type === "pattern" && (
              <li>Name must be the letter not number</li>
            )}
            {errors.password?.type === "required" && (
              <li>Password is required</li>
            )}
            {errors.password?.type === "minLength" && (
              <li>Password must be 6 characters long</li>
            )}
            {errors.email?.type === "pattern" && <li>Invalid Email Address</li>}
            {errors.email?.type === "required" && <li>Email is required</li>}
          </ul>
        )}
        <button className="bg-blue-500" type="submit">
          Xác nhận
        </button>
      </form>
    </div>
  );
};
export default User;
