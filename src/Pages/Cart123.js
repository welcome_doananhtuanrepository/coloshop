import React from "react";
import { AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";
import { useSelector, useDispatch } from "react-redux";
import {
  incrementQuantity,
  decrementQuantity,
  removeItem,
  resetItem,
} from "../Redux/cartSlice";
import { MdDelete } from "react-icons/md";
import "./Cart123.css";
import { Link } from "react-router-dom";
const Cartupdate = () => {
  let itemCart = useSelector((state) => state.cart.cart);
  const dispatch = useDispatch();
  const handleDecrease = (item) => {
    dispatch(decrementQuantity(item));
  };
  const handleIncrease = (item) => {
    dispatch(incrementQuantity(item));
  };
  const handleDel = (item) => {
    dispatch(removeItem(item));
  };
  const handleSubmit = () => {
    alert("Đơn hàng của bạn sẽ được giao trong 5-7 ngày. Cảm ơn!");
    dispatch(resetItem([]));
  };
  const totalItemPrice = itemCart.reduce((a, b) => a + b.price * b.qty, 0);
  return (
    <div className="wrapper_cart">
        <div>
          <div className="title_cart">
            <h1>Giỏ Hàng</h1>
          </div>
          <div className="top_cart">
            <button><Link to='/products'>Tiếp tục mua sắm</Link></button>
          </div>
          {itemCart.length === 0 ? (
                <div style={{ marginTop: "30px", textAlign:"center"}}>
                <h4>Giỏ hàng hiện đang rỗng</h4>
                </div>
            ):
          (<div className="bottom_cart">
            <div className="info_cart">
            {itemCart.map((item,index)=>(
                <div className="product_cart">
                <div className="productDetail_cart">
                  <img
                    src={item.image[0]}
                    alt=""
                  />
                  <div className="details_cart">
                    <div className="productName_cart">
                      <b>Product:</b> {item.title}
                    </div>
                    <div className="productID_cart">
                      <b>ID:</b> {item.id}
                    </div>
                    <div className="productColor_cart" color="black" />
                    <div className="productSize_cart">
                      <b>Size:</b> {item.size[item.indexSize]}
                    </div>
                  </div>
                  <div className="price_cart">
                    <div className="ProductAmountContainer">
                      <div style={{ cursor: "pointer" }} onClick={()=>handleDecrease(item)}>
                        <AiOutlineMinus />
                      </div>

                      <div className="ProductAmount">{item.qty}</div>
                      <div style={{ cursor: "pointer" }} onClick={()=>handleIncrease(item)}>
                        <AiOutlinePlus />
                      </div>
                    </div>
                    <div className="ProductPrice">$ {item.qty*item.price}</div>
                  </div>
                  <div style={{ cursor: "pointer" }} onClick={()=>handleDel(item)}>
                    <MdDelete size={"18px"} />
                  </div>
                </div>
                <hr />
              </div>
            ))}
            </div>
            <div className="Summary">
              <h1 className="SummaryTitle">CHI TIẾT HÓA ĐƠN</h1>
              <div className="SummaryItem">
                <div className="SummaryItemText">Phí sản phẩm:</div>
                <div className="SummaryItemPrice">$ {totalItemPrice}</div>
              </div>
              <div className="SummaryItem">
                <div className="SummaryItemText">Phí vận chuyển:</div>
                <div className="SummaryItemPrice">$ 5000</div>
              </div>

              <div className="SummaryItem" type="total">
                <div className="SummaryItemText">Tổng cộng:</div>
                <div className="SummaryItemText">$ {totalItemPrice+5000}</div>
              </div>
              <button className="checknow" onClick={handleSubmit}>THANH TOÁN</button>
            </div>
          </div>)}
        </div>
    </div>
  );
};

export default Cartupdate;
