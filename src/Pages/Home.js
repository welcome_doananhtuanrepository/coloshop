import React from "react";
import Carousel from "../component2/Carousel/Carousel"
import Banner from "../component2/Banner/Banner";
import Deal from "../component2/Deal/Deal";
import BestSeller from "../component2/BestSeller/BestSeller";
import Benefit from "../component2/Benefit/Benefit";
import Blogg from "../component2/Blog/Blog";
import Arrival from "../component2/Arrival/Arrival";
import Cartupdate from "./Cart123";
const Home = () => {
    return ( 
        <>
            <Carousel/>
            <Banner/>
            <Arrival/>  
            <Deal/>
            <BestSeller/>
            <Benefit/>
            <Blogg/>
            {/* <Cartupdate/> */}
        </>
     );
}
 
export default Home;