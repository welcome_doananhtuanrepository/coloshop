// import './App.css';
import Navbar from "./component2/Navbar/Navbar"
import Newsletter from './component2/Newsletter/Newsletter';
import Footer from './component2/Footer/Footer';
import {BrowserRouter, Routes, Route} from "react-router-dom"
import Home from './Pages/Home';
import Products from './Pages/Products';
import Product from './Pages/Product';
import Cartupdate from "./Pages/Cart123"
import { store } from './Redux/store'
import { Provider } from 'react-redux'
import User from "./component2/User/User"
function App() {
  return (
    <div>
      <Provider store={store}>
      <BrowserRouter>
        <Navbar/>
        <Routes>
            <Route path='/' element={<Home/>}></Route>
            <Route path='/products' element={<Products/>}></Route>
            <Route path='/product' element={<Product/>}></Route>
            <Route path='/cart' element={<Cartupdate/>}></Route>
            <Route path='/user' element={<User/>}></Route>
        </Routes>
        <Newsletter/>
        <Footer/>
      </BrowserRouter>
      </Provider>
        
    </div>
  );
}

export default App;
