import { createSlice } from '@reduxjs/toolkit'

export const cartSlice = createSlice({
  name: 'cart',
  initialState:{cart:[]},
  reducers: {
    addToCart: (state, action) => {
      const itemInCart = state.cart.find((item) => item.id === action.payload.id);
      if (itemInCart) {
        itemInCart.qty++;
      } else {
        state.cart.push({ ...action.payload, qty: 1 });
      }
    },
    incrementQuantity: (state, action) => {
      const item = state.cart.find((item) => item.id === action.payload.id);
      item.qty++;
    },
    decrementQuantity: (state, action) => {
      const item = state.cart.find((item) => item.id === action.payload.id);
      if (item.qty === 1) {
        item.qty = 1
      } else {
        item.qty--;
      }
    },
    removeItem: (state, action) => {
      const removeItem = state.cart.filter((item) => item.id !== action.payload.id);
      state.cart = removeItem;
    },
    resetItem:(state,action)=>{
      state.cart=action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { addToCart, incrementQuantity, decrementQuantity, removeItem,resetItem} = cartSlice.actions

export default cartSlice.reducer